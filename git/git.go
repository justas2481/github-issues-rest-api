// Git related functions.
package git

import (
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"
)

const (
	// Base address for Git repos.
	baseGitAddr = "github.com" // Github
)

// Retrieves repo name of a remote git repository
// parsed from URL of a current working directory.
func GetRepo() (string, error) {
	cwd, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf(
			"can not retrieve current working directory",
		)
	}
	gitDir, err := os.Stat(cwd + "/.git")
	if os.IsNotExist(err) || err != nil || !gitDir.IsDir() {
		return "", fmt.Errorf(
			"it seams that directory '" + cwd + "' has no Git repository.",
		)
	}
	out, err := exec.Command(
		"git",
		"config",
		"--get",
		"remote.origin.url",
	).Output()
	if err != nil {
		return "", fmt.Errorf(
			"it seems that this repository has no remote repository attached",
		)
	}

	url := string(out)
	if !strings.Contains(url, baseGitAddr) {
		return "", fmt.Errorf(
			"repository has no remote URL which references the Git repo manager like %s",
			baseGitAddr,
		)
	}

	return strings.Split(path.Base(url), ".")[0], nil
}
