// Simple Github issue manager.
// It allows to search, create, edit, preview and resolve issues
// in a command line.
package main

import (
	"fmt"
	"github/cmdproc"
)

const (
	// Name of a CMD parameter which controls whether input of an issue
	// should be entered as field or within the file.
	longIssue = "--long"
)

func main() {
	handlers := []*cmdproc.CommandHandler{
		{
			CMD: "search",
			FN:  searchFN,
		},
		{
			CMD: "create",
			FN:  createFN,
		},
		{
			CMD: "list",
			FN:  readFN,
		},
		{
			CMD: "update",
			FN:  updateFN,
		},
		{
			CMD: "lock",
			FN:  lockFN,
		},
	}

	if ok := cmdproc.ProcessCommand(cmdproc.GetCommand(), handlers); !ok {
		fmt.Println(
			"Unknown command",
			cmdproc.GetCommand(),
		)
		return
	}
}
