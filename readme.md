# Github Issues API Client

This project was created in order to learn Golang, specifically to work with HTTP requests and responses, JSON marshaling and unmarshaling, struct fields tagging, external process execution via os/exec package, REST in general and more.

## Features

1. Search within Github public repositories via [Search API](https://docs.github.com/en/rest/search) by following it's rules.
1. Create, read, update and lock your own repository issues by utilizing [Github Issues API](https://docs.github.com/en/rest/issues).

## Usage and implementation details

* This REST client uses Git and must be run from the project root directory to determine a URL of the remote repository;
* Username / owner of a repository must be set as an environment variable called **GITHUB_API_USER**;
* Since authorization happens by using Personal Access Tokens, you must obtain one in Github's settings. Explanation in detail can be found [here](https://docs.github.com/en/enterprise-server@3.4/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token);
* Personal Access Token must be set as an environment variable called **GITHUB_API_PAT**;
* Application must be in path of **$PATH** environment variable.

### Commands

1. **create**: creates an issue with a given title and body, both are entered after issuing the command. If **--long** flag is present after the command, body of an issue is entered in one of the found external editors within the system. Nano and Vim are currently supported. If more editors should be supported, app might be extended with ease.
1. **list**: lists newest owner issues following API defaults.
1. **update**: asks to enter a number of the issue, then prompts to enter an issue details (title and body). If **--long** flag is set, body of an issue is entered within external editor.
1. **lock**: locks an issue by marking it as **resolved**. After command is issued, user is prompted to enter an issue number.
1. **search query**: searches for the issues within public Github repositories. Data presented as an output is displayed not in the tidiest manner, but firstly it lists newest issues (less than one month old), then lists all of them sorted by the date created in descendant order. Functionality can be extended to display grouped issues less than a year old or greater than a year old, because internals are written for that, only display handleing is required.

## Considerations

There are no Unit and functional tests, benchmarking or good customizability for the end user. Refactoring perhaps could be done better, but benefit is considerable, especially because of the small tweeks in each API end-point or a particular method with a specified request body. Templates would be benefitial if project would go to production.