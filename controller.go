// Controller for a program entrypoint.
package main

import (
	"fmt"
	"github/cmdproc"
	"github/git"
	"github/issues"
	"github/issues/crud"
	"github/issues/search"
	"log"
	"os"
	"strconv"
)

// Search for an issue acording to Github's rules.
var searchFN cmdproc.Handler = func() {
	issuesRes, err := search.SearchForIssues(os.Args[2:])
	if err != nil {
		fmt.Fprintf(
			os.Stderr,
			"Error when trying to retrieve issues.\nDetails:\n%v\n",
			err,
		)
		return
	}
	// Sort issues by createdAt descendant order
	// that they would fall in order within categories as well
	issuesRes.SortByTime(true)
	cats := search.ItemsAsAgeCats(issuesRes.Items)
	fmt.Printf(
		"Issues that are %s old:\n",
		search.LessMonth,
	)
	lessMonth, ok := search.GetItemsByCat(cats, search.LessMonth)
	if ok {
		for _, issue := range lessMonth {
			fmt.Printf(
				"%s (created at %s)\n",
				issue.Title,
				issues.FormatTime(issue.CreatedAt),
			)
		}
	}
	fmt.Println(
		"All found issues:",
	)
	fmt.Println(issuesRes)
}

// Create an issue for a remote repo.
var createFN cmdproc.Handler = func() {
	issue := getNewIssue()
	setIssueData(issue)
	if err := issue.Create(); err != nil {
		fmt.Printf(
			"Error had occured. %s\n.",
			err,
		)
		return
	}
	fmt.Println("Issue created.")
}

// Asks for an issues of a current user and displays them.
var readFN cmdproc.Handler = func() {
	issue := getNewIssue()
	repoIssues, err := issue.Read()
	if err != nil {
		fmt.Fprintf(
			os.Stderr,
			"%s\n",
			err,
		)
	}

	// Display found issues
	issuesTotal := len(repoIssues)
	// Tracks if at least one issue has been found
	actualIssue := false
	for i, val := range repoIssues {
		// Filter out issues which are pull requests
		if val.Pr != nil {
			continue
		}

		// Add new lines if at least one issue is present and current issue is not last
		if actualIssue && i < issuesTotal-1 {
			fmt.Printf(
				"\n\n",
			)
		}
		actualIssue = true
		fmt.Printf(
			"%d. %s, issue #%d: %s\n%s\n%s",
			i+1,
			issues.FormatTime(val.CreatedAt),
			val.Number,
			val.Title,
			val.Body,
			val.HTMLURL,
		)
	}
}

// Update an issue identified by its number.
var updateFN cmdproc.Handler = func() {
	issue := getNewIssue()
	issue.Number = numberField()
	setIssueData(issue)

	if err := issue.Update(); err != nil {
		fmt.Printf(
			"Error had occured. %s\n.",
			err,
		)
		return
	}
	fmt.Println("Issue updated.")
}

// Lock an issue (mark it as resolved) identified by its number.
var lockFN cmdproc.Handler = func() {
	issue := getNewIssue()
	issue.Number = numberField()

	if err := issue.Lock(); err != nil {
		fmt.Printf(
			"Error had occured. %s\n.",
			err,
		)
		return
	}
	fmt.Println("Issue locked.")
}

// Helper function to retrieve an issue.
//
// Avoiding DRY violation.
func getNewIssue() *crud.Issue {
	repo, err := git.GetRepo()
	if err != nil {
		log.Fatal(
			err,
		)
	}

	issue, err := crud.NewIssue(issues.GetAuthUsername(), repo)
	if err != nil {
		fmt.Fprintf(
			os.Stderr,
			"%s\n",
			err,
		)
		os.Exit(1)
	}

	return issue
}

// Sets issue data by calling inner methods.
//
// This function helps to avoid DRY violation.
func setIssueData(issue *crud.Issue) {
	issue.Title = cmdproc.ReadField("Issue title:")
	if cmdproc.GetParams()[0] == longIssue {
		contents, err := cmdproc.InvokeEditor()
		if err != nil {
			log.Fatal(
				err,
			)
		}

		issue.Body = contents
	} else {
		issue.Body = cmdproc.ReadField("Description of an issue:")
	}
}

// Read issue number from an input.
func numberField() int {
	var err error
	var number int
	for {
		number, err = strconv.Atoi(cmdproc.ReadField("Issue number:"))
		if err != nil {
			fmt.Println(
				"Issue number can consist of arabic digits only!",
			)
			continue
		}

		break
	}

	return number
}
