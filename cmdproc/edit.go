// Type an text within an editor instead of standard console.
package cmdproc

import (
	"fmt"
	"os"
	"os/exec"
)

const (
	// Prefix for a temporary file to be able to distinct files from others in temp.
	filenamePrefix = "github_api_content"
)

// Editors that could be invoked in a sequential order if prev is unavailable.
var editors = [...]string{"nano", "vim"}

// Invokes an editor.
//
// Waits while user enters data and saves contents to the file
// or until user cancels process.
// InvokeEditor should be used for a temp file which ment to be auto delt with .
func InvokeExternalEditor(editor, filename string) error {
	// Invoke an editor
	cmd := exec.Command(editor, filename)
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	if err := cmd.Start(); err != nil {
		return fmt.Errorf(
			"can not start an external editor",
		)
	}

	if err := cmd.Wait(); err != nil {
		return fmt.Errorf(
			"process associated with an external editor failed",
		)
	}

	return nil
}

// Returns the name of an editor which is found as available in os PATH.
//
// If no editor of the editors is available, function returns an empty string.
func FindAvailableEditor() string {
	for _, editor := range editors {
		if _, err := exec.LookPath(editor); err == nil {
			return editor
		}
	}

	return ""
}

// Creates temp file,
// invokes one of the supported external editors with temp file,
// waits for the editor to be closed,
// reads contents from the saved file (if user saves it),
// removes temp file and returns file contents.
func InvokeEditor() (string, error) {
	editor := FindAvailableEditor()
	if editor == "" {
		return "", fmt.Errorf(
			"app can not find the supported editor to be invoked",
		)
	}

	tempDir := os.TempDir()
	if tempDir == "" {
		return "", fmt.Errorf(
			"unable to retrieve temp directory",
		)
	}

	tempFile, err := os.CreateTemp(tempDir, filenamePrefix)
	if err != nil {
		return "", fmt.Errorf(
			"unable to create a temp file",
		)
	}
	defer removeFile(tempFile.Name())

	if err := InvokeExternalEditor(editor, tempFile.Name()); err != nil {
		return "", err
	}

	contents, err := os.ReadFile(tempFile.Name())
	if err != nil {
		return "", fmt.Errorf(
			"can not read contents of a temp file",
		)
	}

	return string(contents), nil
}

// Removes given file on success or panics on failure.
func removeFile(filename string) {
	if err := os.Remove(filename); err != nil {
		panic(err)
	}
}
