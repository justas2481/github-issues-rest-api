// Invoke commands as functions via use of handlers.
// Invoke a text editor for entering data.
// Mechanism for reading strings as one line fields.
package cmdproc

import (
	"bufio"
	"fmt"
	"os"
)

// Handler declaration.
// This is how simple function as a handler for the command must be declared.
type Handler func()

// Struct of which instances will be sent to process commands with handlers.
type CommandHandler struct {
	// Command name.
	CMD string

	// Function to be executed if given cmd is triggered.
	FN Handler
}

// Process retrieved command.
//
// If ok is false, then command is not found in handlers list.
func ProcessCommand(cmd string, funcs []*CommandHandler) (ok bool) {
	for _, cmdH := range funcs {
		if cmdH.CMD == cmd {
			cmdH.FN()
			return true
		}
	}

	return false
}

// Get command to invoke.
func GetCommand() string {
	// Check if there is only a path to the binary, or command is attached as well
	if len(os.Args) < 2 {
		return ""
	}

	return os.Args[1]
}

// Get command params.
//
// If params are returned as nil, then there are no params.
func GetParams() []string {
	// Check if params are set
	if len(os.Args) > 2 {
		return os.Args[2:]
	}

	return make([]string, 1)
}

// Reads a field from console.
//
// Feald is considered to be a string read until line feed ('\n') is found.
func ReadField(prompt string) string {
	buff := bufio.NewReader(os.Stdin)
	var field string
	var err error
	for {
		fmt.Println(prompt)
		field, err = buff.ReadString('\n')
		fieldLen := len(field)
		if err == nil && fieldLen > 1 {
			field = field[0 : fieldLen-1]
			break
		}
	}

	return field
}
