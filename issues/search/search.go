// Search for issues acording to Github API rules.
package search

import (
	"encoding/json"
	"fmt"
	"github/issues"
	"io"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

const (
	// Search endpoint.
	SearchURL = issues.APIURL + "search/issues"

	// Constants for the periods of time to group by
	LessMonth   = "less than month"
	LessYear    = "less than a year"
	GreaterYear = "greater than a year"
	// Change it in acordance to constants of time periods
	TotalPeriods = 3
)

type User struct {
	ID int

	Username string `json:"login"`

	HTMLURL string `json:"html_url"`
}

type IssueLabel struct {
	ID int

	Name string
}

type Item struct {
	HTMLURL string `json:"html_url"`

	State string

	CreatedAt time.Time `json:"created_at"`

	UpdatedAt time.Time `json:"updated_at"`

	Title string

	Body string

	User User

	IssueLabel []*IssueLabel `json:"labels"`
}

type IssuesResult struct {
	TotalCount int `json:"total_count"`

	IncompleteResults bool `json:"incomplete_results,omitempty"`

	Items []*Item
}

// Default functionality for printing an issue.
func (res *IssuesResult) String() string {
	var buff strings.Builder
	var labelBuff strings.Builder
	itemsLen := len(res.Items)
	if itemsLen == 0 {
		return "No issues found acording to the given criteria."
	}

	for i, item := range res.Items {
		labelCount := len(item.IssueLabel)
		for j, label := range item.IssueLabel {
			labelBuff.WriteString(label.Name)
			if j != labelCount-1 {
				labelBuff.WriteString(", ")
			} else {
				labelBuff.WriteByte('.')
			}
		}
		buff.WriteString(
			fmt.Sprintf(
				"Title: %s\nState: %s\nCreated at: %s\nUpdated at: %s\nUser: %s (%s)\nIssue URL: %s\nLabels: %s",
				item.Title,
				item.State,
				item.CreatedAt,
				item.UpdatedAt,
				item.User.Username,
				item.User.HTMLURL,
				item.HTMLURL,
				labelBuff.String(),
			),
		)
		/// Not the last issue, separate by new lines
		if i < itemsLen-1 {
			buff.WriteString("\n\n")
		}
		labelBuff.Reset()
	}

	return buff.String()
}

// Sorts issues by the date created, time included
// from newest to oldest or vice versa.
func (res *IssuesResult) SortByTime(desc bool) {
	sort.Slice(
		res.Items,
		func(i, j int) bool {
			iTime := res.Items[i].CreatedAt.Unix()
			jTime := res.Items[j].CreatedAt.Unix()
			if desc {
				return iTime > jTime
			}

			return iTime < jTime
		})
}

// Searches for issues acording to rules
// Given here:
// https://docs.github.com/en/rest/reference/search
// in Search issues and pull requests section.
func SearchForIssues(query []string) (*IssuesResult, error) {
	resp, err :=
		http.Get(
			SearchURL + "?q=" + url.QueryEscape(strings.Join(query, " ")),
		)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(
			"cannot issue a request. status: %s",
			resp.Status,
		)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var data IssuesResult
	if err := json.Unmarshal(body, &data); err != nil {
		return nil, fmt.Errorf(
			"unable to decode response as json. error: %v",
			err,
		)
	}

	return &data, nil
}

// Get issues mapped in categories of less than month,
// less than year,
// greater than a year.
func ItemsAsAgeCats(items []*Item) map[string][]*Item {
	cats := make(map[string][]*Item, TotalPeriods)
	now := time.Now().Unix()
	lessMonth, lessYear := now-(30*24*3600), now-(365*24*3600)
	for _, item := range items {
		var key string
		itemTime := item.CreatedAt.Unix()
		switch {
		case lessMonth <= itemTime:
			key = LessMonth
		case lessYear <= itemTime:
			key = LessYear
		default:
			key = GreaterYear
		}
		cats[key] = append(cats[key], item)
	}

	return cats
}

// Get items by category if it exists.
func GetItemsByCat(cats map[string][]*Item, cat string) ([]*Item, bool) {
	if val, ok := cats[cat]; ok {
		return val, true
	}

	return nil, false
}
