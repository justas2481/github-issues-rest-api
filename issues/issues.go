// Package that incapsulates all things that are in common for issues.
package issues

import (
	"fmt"
	"net/http"
	"os"
	"time"
)

const (
	// Base URL for the Github API.
	APIURL = "https://api.github.com/"

	// Environment variable name of the Username of the remote repo owner account.
	APIAuthUserVar = "GITHUB_API_USER"

	// Environment variable name which is used to
	// hold an Authorization token.
	APIAuthTokenVar = "GITHUB_API_PAT"

	// API headers for HTTP request:
	APIHeaderAccept        = "Accept"
	APIHeaderAuthorization = "Authorization"
)

// Authorization headers that can be sent to an API attached to HTTP request.
var APIAuthHeader map[string]string

// Set auth headers when package is imported.
func init() {
	APIAuthHeader = map[string]string{
		APIHeaderAccept:        "application/vnd.github.v3+json",
		APIHeaderAuthorization: "token " + GetAuthToken(),
	}
}

// Returns authorization headers for an API.
func GetAuthHeader() http.Header {
	h := http.Header{}
	for key, val := range APIAuthHeader {
		h.Set(key, val)
	}

	return h
}

// Reads Authorization token from ENV variable.
func GetAuthToken() string {
	return os.Getenv(APIAuthTokenVar)
}

// Reads Authorization username from ENV variable.
func GetAuthUsername() string {
	return os.Getenv(APIAuthUserVar)
}

// Helper function to format time acording to the preference.
func FormatTime(t time.Time) string {
	return fmt.Sprintf(
		"%d-%02d-%02d %02d:%02d:%02d",
		t.Year(),
		t.Month(),
		t.Day(),
		t.Hour(),
		t.Minute(),
		t.Second(),
	)
}
