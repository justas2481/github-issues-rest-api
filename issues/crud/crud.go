// Create, read, update and lock operations for an issue.
package crud

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github/issues"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

// Needed just for checking if issue is pull request or a regular issue.
type pullRequest struct {
	HTMLURL string `json:"html_url"`
}

type Issue struct {
	owner string

	repo string

	Number int `json:"number"`

	Title string `json:"title"`

	Body string `json:"body"`

	HTMLURL string `json:"html_url"`

	Pr *pullRequest `json:"pull_request"`

	CreatedAt time.Time `json:"created_at"`
}

// Constructor for a new Issue.
//
// If owner or repo is empty or becomes empty after encoding them as path URL,
// function will return an error.
func NewIssue(owner, repo string) (*Issue, error) {
	owner = url.PathEscape(owner)
	repo = url.PathEscape(repo)
	if len(owner) < 1 || len(repo) < 1 {
		return nil, fmt.Errorf(
			"invalid owner or repository",
		)
	}

	return &Issue{
		owner: owner,
		repo:  repo,
	}, nil
}

// Create new issue.
//
// Rule: POST /repos/{owner}/{repo}/issues.
func (is *Issue) Create() error {
	if len(is.Title) < 1 || len(is.Body) < 1 {
		return fmt.Errorf(
			"neither issue title nor body can be empty",
		)
	}

	createURL := issues.APIURL +
		"repos/" +
		is.getOwner() + "/" +
		is.GetRepo() + "/issues"

	data, err := json.Marshal(*is)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(
		http.MethodPost,
		createURL,
		bytes.NewBuffer(data),
	)
	if err != nil {
		return err
	}

	req.Header = issues.GetAuthHeader()
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf(
			"HTTP status error. Status: %d",
			resp.StatusCode,
		)
	}

	return nil
}

// Get newest Github issues of the current repo and the current owner
// which is determined by the remote repo URL
// of the current working directory's Git data
// and the ENV var which sets the user.
//
// Rule: GET /repos/{OWNER}/{REPO}/issues.
func (is *Issue) Read() ([]*Issue, error) {
	var err error
	readURL := issues.APIURL +
		"repos/" +
		is.getOwner() + "/" +
		is.GetRepo() + "/issues"
	req, err := http.NewRequest(
		http.MethodGet,
		readURL,
		nil,
	)
	if err != nil {
		return nil, err
	}

	req.Header = issues.GetAuthHeader()
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(
			"HTTP status error. Status: %d",
			resp.StatusCode,
		)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf(
			"can not read the body of a response: %v",
			err,
		)
	}

	var issuesList []*Issue
	if err = json.Unmarshal(body, &issuesList); err != nil {
		return nil, fmt.Errorf(
			"unable to decode JSON response",
		)
	}

	return issuesList, nil
}

// Update an existing issue.
//
// Rule: PATCH /repos/{owner}/{repo}/issues/{issue_number}.
func (is *Issue) Update() error {
	if len(is.Title) < 1 ||
		len(is.Body) < 1 ||
		is.Number == 0 {
		return fmt.Errorf(
			"Issue title, body and number cannot be empty",
		)
	}

	updateURL := issues.APIURL +
		"repos/" +
		is.getOwner() + "/" +
		is.GetRepo() + "/issues/" +
		url.PathEscape(strconv.Itoa(is.Number))

	data, err := json.Marshal(*is)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(
		http.MethodPatch,
		updateURL,
		bytes.NewBuffer(data),
	)
	if err != nil {
		return err
	}

	req.Header = issues.GetAuthHeader()
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf(
			"HTTP status error. Status: %d",
			resp.StatusCode,
		)
	}

	return nil
}

// Lock an existing issue.
//
// In this case, locking means marking an issue as resolved.
//
// Rule: PUT /repos/{owner}/{repo}/issues/{issue_number}/lock.
func (is *Issue) Lock() error {
	if is.Number == 0 {
		return fmt.Errorf(
			"Issue number cannot be empty",
		)
	}

	lockURL := issues.APIURL +
		"repos/" +
		is.getOwner() + "/" +
		is.GetRepo() + "/issues/" +
		url.PathEscape(strconv.Itoa(is.Number)) + "/lock"

	req, err := http.NewRequest(
		http.MethodPut,
		lockURL,
		bytes.NewBuffer(
			[]byte(`{"lock_reason":"resolved"}`),
		),
	)
	if err != nil {
		return err
	}

	req.Header = issues.GetAuthHeader()
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf(
			"HTTP status error. Status: %d",
			resp.StatusCode,
		)
	}

	return nil
}

// Get owner of a repository.
func (is *Issue) getOwner() string {
	return is.owner
}

// Get name of a repository.
func (is *Issue) GetRepo() string {
	return is.repo
}
